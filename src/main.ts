import Koa from 'koa';

import {PORT} from './@utils/env';

import {powerAppMiddleware} from './power-app';
import {router} from './router';

const app = new Koa();

app.use(powerAppMiddleware);

app.use(router.routes()).use(router.allowedMethods());

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
