import {PowerApp} from '@makeflow/power-app';

import {koaAdapter} from '@makeflow/power-app-koa';

export const powerApp = new PowerApp();

powerApp.version('1.0.0', {
  installation: {},
  contributions: {},
});

export const powerAppMiddleware = powerApp.middleware(koaAdapter, {
  path: '/makeflow',
});
