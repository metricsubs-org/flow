import Router from '@koa/router';

export const router = new Router();

router.get('/', ctx => {
  ctx.body = JSON.stringify({
    name: 'MetricSubs Flow',
    version: '1.0.0',
  });
});

router.get('/health', ctx => {
  ctx.body = JSON.stringify({
    status: 'UP',
  });
});
